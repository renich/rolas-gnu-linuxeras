Hazlo en C
==========

:Date: 2023-03-18
:Version: 1
:Authors:
    - Iván Chavero
    - Renich Bon Ćirić
:Original artist: Paul McCarthy
:Description:
    Esta rola es una traducción de otra rola; que es una adaptación de "Let it be"; por Paul McCarthy. A final de cuentas,
    es nomás una actualización de la rola para mencionar lenguajes de programación actuales.

::

    # A
    Cuando mi programa tiene pedos,
    compas me vienen a ver,
    con palabras sabias:
    "hazlo en C".

    Y con la entrega aquí en puerta,
    hay tantos bugs sin resolver,
    alguien manda un inbox / alguien me twittea:
    "hazlo en C".

    # B
    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    rust no está tan vergas,
    hazlo en C.

    # A
    Solía hacerlo todo en FORTRAN
    bueno si chambeas en CERN,
    pruébalo con GUIs...
    hazlo en C.

    Ya voy por casi 30 horas,
    pinche assembler ya me harté,
    pronto entenderás...
    hazlo en C.

    # B
    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    BASIC's pa' puñetas,
    hazlo en C.

    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    python es muy lento,
    hazlo en C.

    # solo

    # B
    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    ECMA es pa' hypsters,
    hazlo en C.

    # A
    Borrosa mi pantalla y vim
    ya no quiere responder,
    Stallman en mi oido:
    "hazo en C".

    Si mucha gente me ha jurado
    que go es el que's más cabrón,
    no tiene ni while loop...
    hazlo en C.

    # B
    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    kotlin aún es java,
    hazlo en C.

    Hazlo en C, hazlo en C,
    hazlo en C, hazlo en C,
    Dart es pa' maricas,
    hazlo en C.

