Mi primer día en GNOME
======================

:Date: 2023-03-18
:Version: 1
:Authors:
    - Renich Bon Ćirić
:Original artist: Enanitos Verdes
:Description:
    Esta rola es una adaptación de "Mi primer día sin tí"; de Enanitos Verdes. Es nomás una adaptación dedicada a los newbies que
    entran al mundo de GNU/Linux; en lo particular, a Fedora.

::

    # C
    Oh, oh, oh, oh oh,
    ye, eh, eh, hea,
    Woh, oh, oh, oh oh,
    yeiee.

    Oh, oh, oh, oh oh,
    ye, eh, eh, hea,
    Woh, oh, oh, oh oh,
    yeiee.

    # A
    No estoy acostumbrado,
    mi primer día en GNOME,
    porque yo antes tenía íconos regados por doquier.

    Cuando instalé Fedora,
    todo parecía bien,
    activé rpmfusion y los codecs instalé.

    # B
    Y mi PC serviría sin mayor complicación,
    no le instalé ni un driver pero todo funcionó... uuuh

    # A
    Olvidé algunas cosas,
    en mi rápido partir,
    No les puse un subvolume a home, var y srv.

    Y esque no batalle nada,
    todo jala super bien,
    se redujo el consumo de recursos del PC.

    # B
    Y en la laptop no jaló el wifi; no supe que pasó,
    y me meto a telegram y lo hicieron funcionar,
    en Fedora México.

    # D
    Porque este es mi primer día en GNOME,
    pues hago en GNU/Linux lo que es mi pasión.
    Y la aplicación se me hace gigante,
    ocupa la pantalla; no hay distracción.
    ¡Ésto está bien chingón!

    # C
    Oh, oh, oh, oh oh,
    ye, eh, eh, hea,
    yeiee.

    Oh, oh, oh, oh oh,
    ye, eh, eh, hea,
    woh, oh, oh, oh oh,
    yeiee.

    # A
    Cuando instalé Fedora,
    todo parecía bien,
    activé rpmfusion y los codecs instalé.

    Y esque no batalle nada,
    todo jala super bien,
    se redujo el consumo de recursos del PC.

    # B
    Y mi PC serviría sin mayor complicación,
    no le instalé ni un driver pero todo funcionó...
    ¡Hasta el bluetooth me jaló!

    # D
    Porque este es mi primer día en GNOME,
    pues hago en GNU/Linux lo que es mi pasión.
    Y la aplicación se me hace gigante,
    ocupa la pantalla; no hay distracción.

    Porque este es mi primer día en Fedora,
    ahora distruto de la libertad.
    ¡Y, con software libre, mi PC es la onda!
    Me siento tan seguro por que no apagué,
    el SELinux, ¡no!

    # C
    Woh, oh, oh, oh oh,
    ye, eh, eh, hea,
    yeiee.

    Oh, oh, oh, oh oh,
    ye, eh, eh, hea,
    woh, oh, oh, oh oh,
    yeiee.

